class Order < ApplicationRecord
    has_one :customer
    has_many :items
    has_many :payments


    validates_presence_of :external_code, :store_id, :sub_total, :delivery_fee, :total_shipping, :total, :dt_order_create,
    :country, :state, :city, :district, :street, :complement, :latitude, :longitude, :postal_code, :number
end
