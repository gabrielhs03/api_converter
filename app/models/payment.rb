class Payment < ApplicationRecord
  belongs_to :order

  validates_presence_of :payment_type, :value

end
