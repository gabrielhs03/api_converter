## Projeto

Sistema para converter dados de uma api para o formato da empresa Delivery Center


## Roadmap

### TDD
- [X] Api
  - [X] Verificar se está recebendo dados da api
  - [X] Verificar se os dados estão com as chaves corretas
- [X] Models
  - [X] Testar se o model está sendo criado
  - [X] Testar se o model está recebendo todos os dados corretamente
- [X] Enviando dados corretamente
  - [X] Verificar se o envio está funcionando corretamente

### Projeto
- [x] Receber dados da API
- [X] Salvar dados nos modelos corretos
- [X] Enviar os dados para o end-point


## Dependências
- Ruby 3.0.0
- Rails 6.1.3.2

## Como rodar
Acessar a pasta do projeto
```bash
cd api_converter/
```

Instalar as gems :
```bash
bundle install
```
Configurar os acessos da base de dados:
```bash
code config/database.yml
```
Criar a base de dados:
```bash
rails db:create db:migrate
```
Rodar o projeto
```bash
rails s
```



## Colaborador

![](https://avatars.githubusercontent.com/u/74117627?s=400&u=209d361847f7a2832ccb6a6354ebe3cdce69d2af&v=4)  
Gabriel Henrique (gabriel.hqs03@gmail.com.br)
