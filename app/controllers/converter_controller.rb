class ConverterController < ApplicationController
    include ActionView::Helpers::NumberHelper
    
    def index
        response = get_payload
        order_received = convert_to_object(response)
        
        generate_model(order_received)

        json_order = convert_order_to_json

        response = send_order_to_api(json_order)
        if response.code == 200
            if @order.save
                render html: "OK #{@order.id}" 
            else
                render html: "Falha ao gravar dados #{@order.errors.each {|e| e } }"
            end
        else
            render html: "#{response}"
        end

    end

    private
    
    def get_payload
        require 'httparty'
        HTTParty.get('http://localhost:3000/payload.json').to_json
    end

    def convert_to_object(response)
        require 'json'

        JSON.parse(response, object_class: OpenStruct)
    end
    

    def convert_order_to_json
        response = @order.to_json(except: [:id, :created_at, :updated_at], 
            include: [
                customer: {only: [:external_code, :name, :email, :contact]},
                items: {except: [:id, :order_id, :product_id, :created_at, :updated_at], 
                    include: {sub_items: { except: [:id, :order_id, :item_id, :created_at, :updated_at]}}
                },
                payments: {only: [:payment_type, :value] }
            ]
        ) 
         
        response.gsub('payment_type', 'type').split('_').collect(&:capitalize).join.gsub('totalShipping', 'total_shipping')
    end

    def send_order_to_api(order)
        date_now = DateTime.now.strftime('%Hh%M - %d/%m/%y')

        HTTParty.post('https://delivery-center-recruitment-ap.herokuapp.com',
            headers: {'X-Sent': "#{date_now}", 'content-type': 'application/json'},
            body: order
        )
    end

    def generate_model(object)
        @order = Order.new(
            external_code: object.id,
            store_id: object.store_id,
            sub_total: number_with_precision(object.total_amount, precision: 2),
            delivery_fee: number_with_precision(object.total_shipping, precision: 2),
            total_shipping: object.total_shipping,
            total: number_with_precision(object.total_amount_with_shipping, precision: 2),
            dt_order_create: object.date_created,
            country: object.shipping.receiver_address.country.id,
            state: select_uf(object.shipping.receiver_address.state.name),
            city: object.shipping.receiver_address.city.name,
            district: object.shipping.receiver_address.neighborhood.name,
            street: object.shipping.receiver_address.street_name,
            complement: object.shipping.receiver_address.comment,
            latitude: Float(object.shipping.receiver_address.latitude),
            longitude: object.shipping.receiver_address.longitude.to_d,
            postal_code: object.shipping.receiver_address.zip_code,
            number: object.shipping.receiver_address.street_number
        )

        @order.build_customer(
            external_code: object.buyer.id.to_s,
            name: "#{object.buyer.first_name} #{object.buyer.last_name}",
            email: object.buyer.email,
            contact: "#{object.buyer.phone.area_code}#{object.buyer.phone.number}"
        )

        order_items = generate_array_items(object.order_items)

        @order.items.build(
            order_items
        )
        
        order_payments = generate_array_payments(object.payments)
        @order.payments.build(
            order_payments
        )    
    end

    def generate_array_items(object)
        items = []
        for item in object
            items.push({
                external_code: item.item.id,
                name: item.item.title,
                price: item.unit_price,
                quantity: item.quantity,
                total: item.full_unit_price,
            })
        end
        items
    end

    def generate_array_payments(object)
        payments = []
        for payment in object
            payments.push({
                payment_type: payment.payment_type.upcase,
                value: payment.total_paid_amount
            })
        end
        payments
    end

    def select_uf(state)
        case state
        when "Acre" 
            "AC"
        when "Alagoas" 
            "AL"
        when "Amapá" 
            "AP"
        when "Amazonas" 
            "AM"
        when "Bahia" 
            "BA"
        when "Ceará" 
            "CE"
        when "Distrito Federal" 
            "DF"
        when "Espírito Santo" 
            "ES"
        when "Goiás" 
            "GO"
        when "Maranhão" 
            "MA"
        when "Mato Grosso" 
            "MT"
        when "Mato Grosso do Sul" 
            "MS"
        when "Minas Gerais" 
            "MG"
        when "Pará" 
            "PA"
        when "Paraíba" 
            "PB"
        when "Paraná" 
            "PR"
        when "Pernambuco" 
            "PE"
        when "Piauí" 
            "PI"
        when "Rio de Janeiro" 
            "RJ"
        when "Rio Grande do Norte" 
            "RN"
        when "Rio Grande do Sul" 
            "RS"
        when "Rondônia" 
            "RO"
        when "Roraima" 
            "RR"
        when "Santa Catarina" 
            "SC"
        when "São Paulo" 
            "SP"
        when "Sergipe" 
            "SE"
        when "Tocantins",
             "TO"
        end
    end
    
end
