class CreateCustomers < ActiveRecord::Migration[6.1]
  def change
    create_table :customers do |t|
      t.references :order, null: false, foreign_key: true
      t.string :external_code
      t.string :name
      t.string :email
      t.string :contact

      t.timestamps
    end
  end
end
