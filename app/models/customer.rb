class Customer < ApplicationRecord
  belongs_to :order

  validates_presence_of :external_code, :name, :email, :contact

  def as_json options
    super(options.merge(except: [:id, :oder_id, :created_at, :updated_at]))
  end
end
