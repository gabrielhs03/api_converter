require 'rails_helper'
require 'httparty'
require 'json'
include ActionView::Helpers::NumberHelper

RSpec.describe "Validates comunication between apis", type: :request do
  describe 'Validates api receiver' do
    before do
      @response = HTTParty.get('http://localhost:3000/payload.json') 
    end

    it 'Return status 200' do
      expect(@response.code).to eql(200)
    end

    it 'Receive required keys' do
      response = JSON.parse(@response.body)
      keys = ['id', 'store_id', 'date_created', 'date_closed', 'last_updated', 'total_amount', 'total_shipping', 
        'total_amount_with_shipping', 'paid_amount', 'expiration_date', 'order_items', 'payments', 'shipping', 
        'status', 'buyer']
      
        expect(keys).to match_array(response.keys)
      end
  end

  describe 'Validates conversion api to object' do
    before do
      @response = HTTParty.get('http://localhost:3000/payload.json') 
    end

    it 'Convert json to object' do
      object = JSON.parse(@response.to_json, object_class: OpenStruct)

      expect(object.class).to be_an(OpenStruct.class)
    end
  end

  describe 'Validates data to endpoint' do
    before do
      @response = HTTParty.get('http://localhost:3000/payload.json')
      object = JSON.parse(@response.to_json, object_class: OpenStruct)

      @order = Order.new(
        external_code: object.id,
        store_id: object.store_id,
        sub_total: number_with_precision(object.total_amount, precision: 2),
        delivery_fee: number_with_precision(object.total_shipping, precision: 2),
        total_shipping: object.total_shipping,
        total: number_with_precision(object.total_amount_with_shipping, precision: 2),
        dt_order_create: object.date_created,
        country: object.shipping.receiver_address.country.id,
        state: 'GO',
        city: object.shipping.receiver_address.city.name,
        district: object.shipping.receiver_address.neighborhood.name,
        street: object.shipping.receiver_address.street_name,
        complement: object.shipping.receiver_address.comment,
        latitude: Float(object.shipping.receiver_address.latitude),
        longitude: object.shipping.receiver_address.longitude.to_d,
        postal_code: object.shipping.receiver_address.zip_code,
        number: object.shipping.receiver_address.street_number
    )

    @order.build_customer(
        external_code: object.buyer.id.to_s,
        name: "#{object.buyer.first_name} #{object.buyer.last_name}",
        email: object.buyer.email,
        contact: "#{object.buyer.phone.area_code}#{object.buyer.phone.number}"
    )

    items = []
        for item in object.order_items
            items.push({
                external_code: item.item.id,
                name: item.item.title,
                price: item.unit_price,
                quantity: item.quantity,
                total: item.full_unit_price,
            })
        end

      @order.items.build(
          items
      )

      payments = []
        for payment in object.payments
            payments.push({
                payment_type: payment.payment_type.upcase,
                value: payment.total_paid_amount
            })
        end

      @order.payments.build(
        payments
    ) 

    @response = @order.to_json(except: [:id, :created_at, :updated_at], 
      include: [
          customer: {only: [:external_code, :name, :email, :contact]},
          items: {except: [:id, :order_id, :product_id, :created_at, :updated_at], 
              include: {sub_items: { except: [:id, :order_id, :item_id, :created_at, :updated_at]}}
          },
          payments: {only: [:payment_type, :value] }
      ]
  ).gsub('payment_type', 'type').split('_').collect(&:capitalize).join.gsub('totalShipping', 'total_shipping') 
        
    end
    
    it 'Data has send without data' do
      response = HTTParty.post('https://delivery-center-recruitment-ap.herokuapp.com')

      expect(response.code).to eql(500)
    end

    it 'Data has send without body' do
      date_now = DateTime.now.strftime('%Hh%M - %d/%m/%y')
      response = HTTParty.post('https://delivery-center-recruitment-ap.herokuapp.com', headers: {'X-Sent': "#{date_now}", 'content-type': 'application/json'})
      expect(response.code).to eql(500)
    end

    it 'Data has send correctly' do
      date_now = DateTime.now.strftime('%Hh%M - %d/%m/%y')
      response = HTTParty.post('https://delivery-center-recruitment-ap.herokuapp.com', headers: {'X-Sent': "#{date_now}", 'content-type': 'application/json'}, body: @response)
      expect(response.code).to eql(200)
    end
  end
end
