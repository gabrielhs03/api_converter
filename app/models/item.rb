class Item < ApplicationRecord
  belongs_to :order

  has_many :sub_items, class_name: 'Item', foreign_key: 'product_id'
  belongs_to :product, class_name: 'Item', optional: true

  validates_presence_of :external_code, :name, :price, :quantity, :total
end
