class CreateItems < ActiveRecord::Migration[6.1]
  def change
    create_table :items do |t|
      t.references :order, null: false, foreign_key: true
      t.string :external_code
      t.string :name
      t.string :price
      t.references :product, foreign_key: { to_table: :items }
      t.integer :quantity
      t.float :total

      t.timestamps
    end
  end
end
