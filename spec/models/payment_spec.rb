require 'rails_helper'
include ActionView::Helpers::NumberHelper

RSpec.describe Payment, type: :model do
  before do
    @response = HTTParty.get('http://localhost:3000/payload.json')
    object = JSON.parse(@response.to_json, object_class: OpenStruct)

    order = Order.create(
      external_code: object.id,
      store_id: object.store_id,
      sub_total: number_with_precision(object.total_amount, precision: 2),
      delivery_fee: number_with_precision(object.total_shipping, precision: 2),
      total_shipping: object.total_shipping,
      total: number_with_precision(object.total_amount_with_shipping, precision: 2),
      dt_order_create: object.date_created,
      country: object.shipping.receiver_address.country.id,
      state: 'GO',
      city: object.shipping.receiver_address.city.name,
      district: object.shipping.receiver_address.neighborhood.name,
      street: object.shipping.receiver_address.street_name,
      complement: object.shipping.receiver_address.comment,
      latitude: Float(object.shipping.receiver_address.latitude),
      longitude: object.shipping.receiver_address.longitude.to_d,
      postal_code: object.shipping.receiver_address.zip_code,
      number: object.shipping.receiver_address.street_number
    )

    for payment in object.payments
      @payment = Payment.new(
        order: order,
        payment_type: payment.payment_type.upcase,
        value: payment.total_paid_amount
      )
    end
  end

  it 'is valid with valid attributes' do
    expect(@payment).to be_valid
  end

  it 'is not valid without valid attributes' do
    @payment.order_id = nil
    expect(@payment).not_to be_valid
  end
end
